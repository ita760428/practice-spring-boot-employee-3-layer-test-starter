package com.afs.restapi;

import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CompanyControllerTest {

    @Autowired
    MockMvc mockMvc;
    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    CompanyRepository companyRepository;

    @BeforeEach
    void setUp() {
        companyRepository.clearAll();
    }

    @Test
    void should_ensure_company_add_companyList_when_perform_addCompany_given_a_company() throws Exception {
        //given
        List<Employee> employeesInDep1 = new ArrayList<>();
        employeesInDep1.add(new Employee(1, "alice", 21, "female", 6000));
        Company companyDep1 = new Company(1,"dep1", employeesInDep1);
        String companyDEp1String = mapper.writeValueAsString(companyDep1);

        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.post("/companies")
                .contentType(MediaType.APPLICATION_JSON)
                .content(companyDEp1String))
                .andExpect(status().isCreated());
        Company companyAdded = companyRepository.getCompanyById(1);
        Assertions.assertNotNull(companyAdded);

    }

    @Test
    void should_get_all_companies_when_perform_get_given_companies() throws Exception {
        //given
        List<Employee> employeesInDep1 = new ArrayList<>();
        employeesInDep1.add(new Employee(1, "alice", 21, "female", 6000));

        List<Employee> employeesInDep2 = new ArrayList<>();
        employeesInDep2.add(new Employee(1, "daisy", 22, "female", 6100));
        employeesInDep2.add(new Employee(2, "ethan", 19, "male", 6000));
        Company companyDep1 = new Company(1, "dep1", employeesInDep1);
        Company companyDep2 = new Company(2, "dep2", employeesInDep2);
        companyRepository.addCompany(companyDep1);
        companyRepository.addCompany(companyDep2);

        //when then
        mockMvc.perform(MockMvcRequestBuilders.get("/companies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(2)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].companyName").isString())
                .andExpect(jsonPath("$[0].companyName").value("dep1"))
                .andExpect(jsonPath("$[0].employees",hasSize(1)))
                .andExpect(jsonPath("$[0].employees[0].id").value(1))
                .andExpect(jsonPath("$[0].employees[0].name").value("alice"))
                .andExpect(jsonPath("$[0].employees[0].age").value(21))
                .andExpect(jsonPath("$[0].employees[0].gender").value("female"))
                .andExpect(jsonPath("$[0].employees[0].salary").value(6000))
                .andExpect(jsonPath("$[1].id").isNumber())
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].companyName").isString())
                .andExpect(jsonPath("$[1].companyName").value("dep2"))
                .andExpect(jsonPath("$[1].employees",hasSize(2)))
                .andExpect(jsonPath("$[1].employees[0].id").value(1))
                .andExpect(jsonPath("$[1].employees[0].name").value("daisy"))
                .andExpect(jsonPath("$[1].employees[0].age").value(22))
                .andExpect(jsonPath("$[1].employees[0].gender").value("female"))
                .andExpect(jsonPath("$[1].employees[0].salary").value(6100))
                .andExpect(jsonPath("$[1].employees[1].id").value(2))
                .andExpect(jsonPath("$[1].employees[1].name").value("ethan"))
                .andExpect(jsonPath("$[1].employees[1].age").value(19))
                .andExpect(jsonPath("$[1].employees[1].gender").value("male"))
                .andExpect(jsonPath("$[1].employees[1].salary").value(6000));
    }


    @Test
    void should_get_a_right_company_when_perform_getEmployeesByCompanyId_given_companies() throws Exception {
        //given
        List<Employee> employeesInDep1 = new ArrayList<>();
        employeesInDep1.add(new Employee(1, "alice", 21, "female", 6000));

        List<Employee> employeesInDep2 = new ArrayList<>();
        employeesInDep2.add(new Employee(1, "daisy", 22, "female", 6100));
        employeesInDep2.add(new Employee(2, "ethan", 19, "male", 6000));
        Company companyDep1 = new Company(1, "dep1", employeesInDep1);
        Company companyDep2 = new Company(2, "dep2", employeesInDep2);
        companyRepository.addCompany(companyDep1);
        companyRepository.addCompany(companyDep2);

        //when then
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.companyName").isString())
                .andExpect(jsonPath("$.companyName").value("dep1"))
                .andExpect(jsonPath("$.employees",hasSize(1)))
                .andExpect(jsonPath("$.employees[0].id").value(1))
                .andExpect(jsonPath("$.employees[0].name").value("alice"))
                .andExpect(jsonPath("$.employees[0].age").value(21))
                .andExpect(jsonPath("$.employees[0].gender").value("female"))
                .andExpect(jsonPath("$.employees[0].salary").value(6000));
    }

    @Test
    void should_get_all_employees_when_perform_getEmployeesByCompanyId_given_companies() throws Exception {
        //given
        List<Employee> employeesInDep1 = new ArrayList<>();
        employeesInDep1.add(new Employee(1, "alice", 21, "female", 6000));

        List<Employee> employeesInDep2 = new ArrayList<>();
        employeesInDep2.add(new Employee(1, "daisy", 22, "female", 6100));
        employeesInDep2.add(new Employee(2, "ethan", 19, "male", 6000));
        Company companyDep1 = new Company(1, "dep1", employeesInDep1);
        Company companyDep2 = new Company(2, "dep2", employeesInDep2);
        companyRepository.addCompany(companyDep1);
        companyRepository.addCompany(companyDep2);

        //when then
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/2/employees"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").value("daisy"))
                .andExpect(jsonPath("$[0].age").value(22))
                .andExpect(jsonPath("$[0].gender").value("female"))
                .andExpect(jsonPath("$[0].salary").value(6100))
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].name").value("ethan"))
                .andExpect(jsonPath("$[1].age").value(19))
                .andExpect(jsonPath("$[1].gender").value("male"))
                .andExpect(jsonPath("$[1].salary").value(6000));
    }


    @Test
    void should_get_matching_companies_when_perform_getCompaniesByPagination_given_companies() throws Exception {
        //given
        List<Employee> employeesInDep1 = new ArrayList<>();
        employeesInDep1.add(new Employee(1, "alice", 21, "female", 6000));

        List<Employee> employeesInDep2 = new ArrayList<>();
        employeesInDep2.add(new Employee(1, "daisy", 22, "female", 6100));
        employeesInDep2.add(new Employee(2, "ethan", 19, "male", 6000));

        List<Employee> employeesInDep3 = new ArrayList<>();
        employeesInDep3.add(new Employee(1, "tom", 22, "male", 8000));

        Company companyDep1 = new Company(1, "dep1", employeesInDep1);
        Company companyDep2 = new Company(2, "dep2", employeesInDep2);
        Company companyDep3 = new Company(3, "dep3", employeesInDep3);
        companyRepository.addCompany(companyDep1);
        companyRepository.addCompany(companyDep2);
        companyRepository.addCompany(companyDep3);

        //when then
        mockMvc.perform(MockMvcRequestBuilders.get("/companies?pageIndex=2&pageSize=2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(3))
                .andExpect(jsonPath("$[0].companyName").isString())
                .andExpect(jsonPath("$[0].companyName").value("dep3"))
                .andExpect(jsonPath("$[0].employees",hasSize(1)))
                .andExpect(jsonPath("$[0].employees[0].id").value(1))
                .andExpect(jsonPath("$[0].employees[0].name").value("tom"))
                .andExpect(jsonPath("$[0].employees[0].age").value(22))
                .andExpect(jsonPath("$[0].employees[0].gender").value("male"))
                .andExpect(jsonPath("$[0].employees[0].salary").value(8000));
    }

    @Test
    void should_return_company_undated_when_perform_updateCompany_given_company() throws Exception {
        //given
        List<Employee> employeesInDep1 = new ArrayList<>();
        employeesInDep1.add(new Employee(1, "alice", 21, "female", 6000));

        List<Employee> employeesInDep2 = new ArrayList<>();
        employeesInDep2.add(new Employee(1, "daisy", 22, "female", 6100));
        employeesInDep2.add(new Employee(2, "ethan", 19, "male", 6000));
        Company companyDep1 = new Company(1, "dep1", employeesInDep1);
        Company companyDep2 = new Company(2, "dep2", employeesInDep2);
        companyRepository.addCompany(companyDep1);
        companyRepository.addCompany(companyDep2);


        Company companyDep1Updated = new Company(1,"dep1dep1",employeesInDep1);
        String companyDep1String = mapper.writeValueAsString(companyDep1Updated);

        //when then
        mockMvc.perform(MockMvcRequestBuilders.put("/companies/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(companyDep1String))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.companyName").isString())
                .andExpect(jsonPath("$.companyName").value("dep1dep1"))
                .andExpect(jsonPath("$.employees",hasSize(1)))
                .andExpect(jsonPath("$.employees[0].id").value(1))
                .andExpect(jsonPath("$.employees[0].name").value("alice"))
                .andExpect(jsonPath("$.employees[0].age").value(21))
                .andExpect(jsonPath("$.employees[0].gender").value("female"))
                .andExpect(jsonPath("$.employees[0].salary").value(6000));

    }

    @Test
    void should_ensure_company_been_deleted_when_perform_remove_given_companies() throws Exception {
        //given
        List<Employee> employeesInDep1 = new ArrayList<>();
        employeesInDep1.add(new Employee(1, "alice", 21, "female", 6000));

        List<Employee> employeesInDep2 = new ArrayList<>();
        employeesInDep2.add(new Employee(1, "daisy", 22, "female", 6100));
        employeesInDep2.add(new Employee(2, "ethan", 19, "male", 6000));
        Company companyDep1 = new Company(1, "dep1", employeesInDep1);
        Company companyDep2 = new Company(2, "dep2", employeesInDep2);
        companyRepository.addCompany(companyDep1);
        companyRepository.addCompany(companyDep2);

        //when
        mockMvc.perform(MockMvcRequestBuilders.delete("/companies/1"))
                .andExpect(status().isNoContent());

        //then
        Company companyDeleted = companyRepository.getCompanyById(1);
        Assertions.assertNull(companyDeleted);

    }


}
