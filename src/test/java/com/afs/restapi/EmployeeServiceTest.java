package com.afs.restapi;

import com.afs.restapi.exception.AgeAndSalaryNotMatchingException;
import com.afs.restapi.exception.IllegalAgeException;
import com.afs.restapi.exception.UpdateStatusError;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.*;


public class EmployeeServiceTest {
    private EmployeeRepository employeeRepository = mock(EmployeeRepository.class);

    private EmployeeService employeeService = new EmployeeService(employeeRepository);

    @Test
    void should_throw_Exception_IllegalAgeException_when_create_employee_given_employee_oder_than_65_years_or_younger_than_18() {
        //give
        Employee employee1 = new Employee(1, "zhangsan", 66, "female", 2000);
        Employee employee2 = new Employee(1, "lisi", 17, "female", 2000);

        //when  then
        Assertions.assertThrows(IllegalAgeException.class,()-> employeeService.addEmployee(employee1));
        Assertions.assertThrows(IllegalAgeException.class,()-> employeeService.addEmployee(employee2));
        verify(employeeRepository,times(0)).insert(employee1);
        verify(employeeRepository,times(0)).insert(employee2);

    }

    @Test
    void should_throw_Exception_AgeAndSalaryNotMatchingException_and_do_not_insert_when_create_employee_given_a_employee_age_bigger_30_but_salary_below_30000() {
        //give
        Employee employee = new Employee(1, "zhangsan", 32, "female", 2000);

        //when  then
        Assertions.assertThrows(AgeAndSalaryNotMatchingException.class,()-> employeeService.addEmployee(employee));
        verify(employeeRepository,times(0)).insert(employee);

    }

    @Test
    void should_change_status_from_default_to_true_when_create_employee_given_a_illegal_employee() {
        //give
        Employee employee = new Employee(1, "zhangsan", 32, "female", 30000);
        Employee employeeToReturn = new Employee(1, "zhangsan", 32, "female", 30000);
        employeeToReturn.setStatus(true);
        when(employeeRepository.insert(employee)).thenReturn(employeeToReturn);

        //when
        Employee employeeSaved = employeeService.addEmployee(employee);

        //then
        Assertions.assertTrue(employeeSaved.isStatus());
        verify(employeeRepository).insert(argThat(employeeToSave ->{
            Assertions.assertTrue(employeeToSave.isStatus());
            return true;
        }));
    }

    @Test
    void should_set_employee_status_to_false_when_delete_a_employee_given_a_employee() {
        //give
        Employee employee = new Employee(1, "zhangsan", 32, "female", 30000);
        employee.setStatus(true);
        when(employeeRepository.findById(1)).thenReturn(employee);

        //when
        Employee employeeBeDeleted = employeeService.deleteEmployeeByChangeStatusToFalse(1);

        //then
        Assertions.assertFalse(employeeBeDeleted.isStatus());
        verify(employeeRepository).findById(Mockito.eq(1));

    }

    @Test
    void should_can_not_update_employeeInfo_when_update_employee_given_a_employee_status_is_false() {
        //give
        Employee employeeToUpdate = new Employee(1, "zhangsan", 32, "female", 30000);
        employeeToUpdate.setStatus(false);
        Employee employeeHasUpdated = new Employee(1, "zhangsan", 32, "female", 30000);
        employeeToUpdate.setStatus(false);
        when(employeeRepository.findById(1)).thenReturn(employeeToUpdate);
        when(employeeRepository.update(1,employeeToUpdate)).thenReturn(employeeHasUpdated);

        //when  then
        Assertions.assertThrows(UpdateStatusError.class,()-> employeeService.updateEmployee(1,employeeToUpdate));
        verify(employeeRepository,times(0)).update(1,employeeToUpdate);
    }
}
