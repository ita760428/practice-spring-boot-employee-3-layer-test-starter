package com.afs.restapi.exception;
public class AgeAndSalaryNotMatchingException extends RuntimeException{
    public AgeAndSalaryNotMatchingException() {
        super("employee age bigger than 30 but salary lower than 20000 can not be create");
    }
}
