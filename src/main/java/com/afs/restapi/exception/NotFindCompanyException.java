package com.afs.restapi.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NotFindCompanyException extends RuntimeException{
    public NotFindCompanyException() {
        super("company id not found");
    }
}
