package com.afs.restapi.exception;

public class UpdateStatusError extends RuntimeException{
    public UpdateStatusError() {
        super("Can't update the employee has left company.");
    }
}
