package com.afs.restapi.exception;

public class IllegalAgeException extends RuntimeException {
    public IllegalAgeException() {
        super("age is illegal!");
    }
}
