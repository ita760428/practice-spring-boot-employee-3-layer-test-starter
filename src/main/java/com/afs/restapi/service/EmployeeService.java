package com.afs.restapi.service;

import com.afs.restapi.exception.AgeAndSalaryNotMatchingException;
import com.afs.restapi.exception.IllegalAgeException;
import com.afs.restapi.exception.UpdateStatusError;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public EmployeeRepository getEmployeeRepository() {
        return employeeRepository;
    }

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getEmployees() {
        return employeeRepository.findAll();
    }

    public Employee addEmployee(int id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> getEmployees(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> getEmployees(int pageNumber, int pageSize) {
        return employeeRepository.findByPage(pageNumber, pageSize);
    }

    public Employee addEmployee(Employee employee) {
        if (employee.getAge() < 18 || employee.getAge() > 65) {
            throw new IllegalAgeException();
        }
        if (employee.getAge() > 30 && employee.getSalary() < 20000) {
            throw new AgeAndSalaryNotMatchingException();
        }
        employee.setStatus(true);
        return employeeRepository.insert(employee);
    }

    public Employee updateEmployee(int id, Employee employee) {
        Employee employeeWillBeUpdate = employeeRepository.findById(id);
        if (employeeWillBeUpdate != null) {
            if(employeeWillBeUpdate.isStatus()){
                employeeRepository.update(id, employee);
            }else{
                throw new UpdateStatusError();
            }

        }
        return employeeWillBeUpdate;
    }

    public void deleteEmployeeById(int id) {
        getEmployeeRepository().delete(id);
    }

    public Employee deleteEmployeeByChangeStatusToFalse(int id) {
        Employee employeeWillBeDelete = employeeRepository.findById(id);
        if (employeeWillBeDelete != null) {
            employeeWillBeDelete.setStatus(false);
        }
        return employeeWillBeDelete;
    }
}