### Objective
今天我们进行了code review，学习了控制反转和依赖注入，然后学习了集成测试。今天下午我们学习了三层架构，  
学习了使用mock进行service层的TDD,然后进行了大量的案例训练。其中我印象最深刻的是学习了使用mock进行  
service层的TDD训练。

### Reflective
difficult

### Interpretive
在今天的学习过程中，我觉得使用mock进行service层的TDD训练是相当有难度的，我一直不理解它的测试流程，  
不明白verify和 when...thenReturn的具体作用。

### Decisional
今天多亏了老师们的耐心讲解，我才逐渐领悟过来，然后顺利通过测试，在今后的学习中我会积极和老师沟通，  
及时反映我的问题的。
